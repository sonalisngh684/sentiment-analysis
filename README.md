**The problem at hand:**
Sentiment analysis (or opinion mining) is a natural language processingtechnique used to determine whether data is positive, negative or neutral. Sentiment analysis is often performed on textual data to help businesses monitor brand and product sentiment in customer feedback, and understand customer needs.

**Sentiment analysis**
It is perhaps one of the most popular 
applications of natural language processing and text 
analytics with a vast number of websites, books and 
tutorials on this subject.

Sentiment analysis is the process of detecting positive or negative sentiment in text. It’s often used by businesses to detect sentiment in social data, gauge brand reputation, and understand customers.

Since customers express their thoughts and feelings more openly than ever before, sentiment analysis is becoming an essential tool to monitor and understand that sentiment. Automatically analyzing customer feedback, such as opinions in survey responses and social media conversations, allows brands to learn what makes customers happy or frustrated, so that they can tailor products and services to meet their customers’ needs.

For example, using sentiment analysis to automatically analyze 4,000+ reviews about your product could help you discover if customers are happy about your pricing plans and customer service.

Maybe you want to gauge brand sentiment on social media, in real time and over time, so you can detect disgruntled customers immediately and respond as soon as possible.

The applications of sentiment analysis are endless. Learn more about how you can out sentiment analysis to use later on in this post.

**Types of Sentiment Analysis:-**

Sentiment analysis models focus on polarity (positive, negative, neutral) but also on feelings and emotions (angry, happy, sad, etc), urgency (urgent, not urgent) and even intentions (interested v. not interested).
Depending on how you want to interpret customer feedback and queries, you can define and tailor your categories to meet your sentiment analysis needs. In the meantime, here are some of the most popular types of sentiment analysis:

Fine-grained Sentiment Analysis
If polarity precision is important to your business, you might consider expanding your polarity categories to include:


- Very positive

- Positive

- Neutral

- Negative

- Very negative

This is usually referred to as fine-grained sentiment analysis, and could be used to interpret 5-star ratings in a review, for example:


- Very Positive = 5 stars

- Very Negative = 1 star

- Emotion detection

This type of sentiment analysis aims to detect emotions, like happiness, frustration, anger, sadness, and so on. Many emotion detection systems use lexicons (i.e. lists of words and the emotions they convey) or complex machine learning algorithms.

One of the downsides of using lexicons is that people express emotions in different ways. Some words that typically express anger, like bad or kill (e.g. your product is so bad or your customer support is killing me) might also express happiness (e.g. this is bad ass or you are killing it).

Aspect-based Sentiment Analysis
Usually, when analyzing sentiments of texts, let’s say product reviews, you’ll want to know which particular aspects or features people are mentioning in a positive, neutral, or negative way. That's where aspect-based sentiment analysis can help, for example in this text: "The battery life of this camera is too short", an aspect-based classifier would be able to determine that the sentence expresses a negative opinion about the feature battery life.

Multilingual sentiment analysis
Multilingual sentiment analysis can be difficult. It involves a lot of preprocessing and resources. Most of these resources are available online (e.g. sentiment lexicons), while others need to be created (e.g. translated corpora or noise detection algorithms), but you’ll need to know how to code to use them.

**Basic terminologies**
- A text corpus consists of multiple text documents and each document can be as simple as a 
single sentence to a complete document with multiple paragraphs. Textual data, in spite of 
being highly unstructured, can be classified into two major types of documents. Factual 
documents that typically depict some form of statements or facts with no specific feelings or 
emotion attached to them. These are also known as objective documents. Subjective 
documents on the other hand have text that expresses feelings, moods, emotions, and 
opinions.
- Sentiment analysis:- is also popularly known as opinion analysis or opinion mining. The key 
idea is to use techniques from text analytics, NLP, Machine Learning, and linguistics to extract 
important information or data points from unstructured text. This in turn can help us derive 
qualitative outputs like the overall sentiment being on a positive, neutral, or negative scale 
and quantitative outputs like the sentiment polarity, subjectivity, and objectivity proportions. 
-** Sentiment polarity**:- is typically a numeric score that’s assigned to both the positive and 
negative aspects of a text document based on subjective parameters like specific words and 
phrases expressing feelings and emotion. Neutral sentiment typically has 0 polarity since it 
does not express and specific sentiment, positive sentiment will have polarity > 0, and 
negative < 0. Of course, you can always change these thresholds based on the type of text 
you are dealing with; there are no hard constraints on this.

**Why Is Sentiment Analysis Important?**

Sentiment analysis is extremely important because it helps businesses quickly understand the overall opinions of their customers. By automatically sorting the sentiment behind reviews, social media conversations, and more, you can make faster and more accurate decisions.

It’s estimated that 90% of the world’s data is unstructured, in other words it’s unorganized. Huge volumes of unstructured business data are created every day: emails, support tickets, chats, social media conversations, surveys, articles, documents, etc). But it’s hard to analyze for sentiment in a timely and efficient manner.

The overall benefits of sentiment analysis include:

**Sorting Data at Scale**

Can you imagine manually sorting through thousands of tweets, customer support conversations, or surveys? There’s just too much business data to process manually. Sentiment analysis helps businesses process huge amounts of data in an efficient and cost-effective way.

**Real-Time Analysis**

Sentiment analysis can identify critical issues in real-time, for example is a PR crisis on social media escalating? Is an angry customer about to churn? Sentiment analysis models can help you immediately identify these kinds of situations, so you can take action right away.

**Consistent criteria**

It’s estimated that people only agree around 60-65% of the time when determining the sentiment of a particular text. Tagging text by sentiment is highly subjective, influenced by personal experiences, thoughts, and beliefs. By using a centralized sentiment analysis system, companies can apply the same criteria to all of their data, helping them improve accuracy and gain better insights.

